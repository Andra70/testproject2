import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(58),
          child: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * .4,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/Rectangle 270.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: MediaQuery.of(context).size.height * .7,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        top: 20,
                        left: 30,
                        right: 30,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          Icon(
                            Icons.star_half,
                            color: Colors.yellow,
                          ),
                          Text("4.5")
                        ],
                      ),
                    ),
                    // Positioned(
                    //   right: 30,
                    //   top: height * 70,
                    //   child: GestureDetector(
                    //     onTap: () => {},
                    //     child: Container(
                    //       height: 60,
                    //       width: 60,
                    //       decoration: BoxDecoration(
                    //           color: Colors.white,
                    //           borderRadius: BorderRadius.circular(10),
                    //           boxShadow: [
                    //             BoxShadow(
                    //                 color: Colors.black.withOpacity(0.5),
                    //                 blurRadius: 5,
                    //                 spreadRadius: 1)
                    //           ]),
                    //       child: const Icon(
                    //         Icons.bookmark_sharp,
                    //         size: 40,
                    //         // color: (like) ? Colors.red : Colors.grey[600],
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    const Padding(
                      padding: EdgeInsets.only(
                        top: 10,
                        left: 30,
                        right: 30,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Test MBTI",
                              style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(
                        top: 10,
                        left: 30,
                        right: 30,
                      ),
                      child: Text(
                        "25 soal • 16personalities.com",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                          color: Color.fromARGB(255, 124, 118, 118),
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: TabBar(
                        tabs: [
                          Text(
                            "Deskripsi",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Photos",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Maps",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Reviews",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                        labelColor: Colors.black,
                        unselectedLabelColor: Colors.grey,
                      ),
                    ),
                    const Expanded(
                      child: TabBarView(
                        children: [
                          // Photos tab content
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum interdum justo, id blandit felis consectetur id. Ut in nunc eu enim rhoncus scelerisque. Mauris convallis dolor ac leo ullamcorper, at pulvinar quam ultricies. Suspendisse potenti. In vulputate sem et sem vestibulum, quis auctor felis bibendum.",
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Text(
                              "Photos",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),

                          // Maps tab content
                          Center(
                            child: Text(
                              "Maps",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),

                          // Reviews tab content
                          Center(
                            child: Text(
                              "Reviews",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: EdgeInsets.only(
                    //     top: 10,
                    //     left: 30,
                    //     right: 30,
                    //   ),
                    //   child: Text(
                    //     "25 soal • 16personalities.com",
                    //     style: TextStyle(
                    //       fontSize: 14,
                    //       fontWeight: FontWeight.normal,
                    //       color: Color.fromARGB(255, 124, 118, 118),
                    //     ),
                    //   ),
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.only(
                    //     top: 20,
                    //     left: 30,
                    //     right: 30,
                    //   ),
                    //   child: Row(
                    //     children: [
                    //       Padding(
                    //         padding: EdgeInsets.only(right: 25.0),
                    //         child: Text(
                    //           "Deskripsi",
                    //           style: TextStyle(
                    //               fontSize: 14,
                    //               fontWeight: FontWeight.normal,
                    //               color: Colors.black),
                    //         ),
                    //       ),
                    //       Padding(
                    //         padding: EdgeInsets.only(right: 25.0),
                    //         child: Text(
                    //           "Photos",
                    //           style: TextStyle(
                    //               fontSize: 14,
                    //               fontWeight: FontWeight.normal,
                    //               color: Colors.black),
                    //         ),
                    //       ),
                    //       Padding(
                    //         padding: EdgeInsets.only(right: 25.0),
                    //         child: Text(
                    //           "Maps",
                    //           style: TextStyle(
                    //               fontSize: 14,
                    //               fontWeight: FontWeight.normal,
                    //               color: Colors.black),
                    //         ),
                    //       ),
                    //       Padding(
                    //         padding: EdgeInsets.only(right: 25.0),
                    //         child: Text(
                    //           "Reviews",
                    //           style: TextStyle(
                    //             fontSize: 14,
                    //             fontWeight: FontWeight.normal,
                    //             color: Colors.black,
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Container(
                          width: 200,
                          child: TextButton(
                            onPressed: () {
                              Navigator.pushNamed(context, '/second');
                            },
                            style: ButtonStyle(
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.white),
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.blue),
                            ),
                            child: const Text(
                              'Mulai Test',
                              style: TextStyle(
                                backgroundColor: Colors.blue,
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
                top: 250,
                right: 45,
                child: GestureDetector(
                    onTap: () => {},
                    child: Container(
                      height: 65,
                      width: 60,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.5),
                                blurRadius: 5,
                                spreadRadius: 1)
                          ]),
                      child: Icon(
                        Icons.bookmark_sharp,
                        size: 40,
                        color: Colors.grey[600],
                      ),
                    ))),
          ],
        ),
      ),
    );
  }
}
