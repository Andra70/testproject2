import 'package:bisaapp4semogabener/ProfilePage.dart';
import 'package:bisaapp4semogabener/CategoryPage.dart';
import 'package:flutter/material.dart';
import 'DetailPage.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70.0),
        child: AppBar(
          title: const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Putri Anindya',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              Text(
                'SMP Bocil Sukses • Kelas 7',
                style: TextStyle(color: Colors.white, fontSize: 13),
              )
            ],
          ),
          leading: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const ProfilePage()));
            },
            child: const Padding(
              padding: EdgeInsets.all(5.0),
              child: CircleAvatar(
                backgroundImage: AssetImage("images/owi.jpg"),
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            // TextButton(
            //   onPressed: () {},
            //   child: Text(
            //     'Calon Pemimpin',
            //     style: TextStyle(
            //       fontSize: 16,
            //       color: Colors.white,
            //       fontWeight: FontWeight.bold,
            //     ),
            //   ),
            //   style: TextButton.styleFrom(
            //     padding: const EdgeInsets.symmetric(
            //         horizontal: 20,
            //         vertical: 5), // Adjusted the vertical padding
            //     shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(50),
            //       side: const BorderSide(
            //           width: 2, color: Colors.white), // Added border
            //     ),
            //   ),
            // ),
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.notifications),
              iconSize: 25,
            ),
          ],
        ),
      ),
      backgroundColor: const Color.fromARGB(255, 33, 150, 243),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 125),
                  SizedBox(
                    height: 100,
                    width: MediaQuery.of(context).size.width - 40,
                    child: const Text(
                      "Cari Tahu Diri mu yang sesungguhnya",
                      style: TextStyle(
                          fontSize: 28,
                          color: Color.fromARGB(255, 231, 231, 231),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(height: 5),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 40,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                      ),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: 'Cari sekolah dan artikel terkait',
                          border: InputBorder.none,
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          prefixIcon: IconButton(
                            icon: const Icon(Icons.search),
                            onPressed: () {
                              showSearch(
                                context: context,
                                delegate: CustomSearch(),
                              );
                            },
                          ),
                          suffixIcon: IconButton(
                            icon: const Icon(Icons.mic),
                            onPressed: () {},
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    margin: const EdgeInsets.all(20),
                    child: Container(
                      height: 200,
                      width: double.infinity,
                      padding: const EdgeInsets.all(20),
                      child: Stack(
                        children: [
                          const Text(
                            'Rekomendasi',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const Positioned(
                            top: 0,
                            right: 0,
                            child: Text(
                              'BISA',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const DetailPage()),
                                );
                              },
                              child: const Text(
                                'Detail Page',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    margin: const EdgeInsets.all(20),
                    child: Container(
                      height: 200,
                      width: double.infinity,
                      padding: const EdgeInsets.all(20),
                      child: Stack(
                        children: [
                          Text(
                            'Rekomendasi',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const Positioned(
                            top: 0,
                            right: 0,
                            child: Text(
                              'BISA',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const CategoryPage()),
                                );
                              },
                              child: const Text(
                                'Category Page',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    margin: const EdgeInsets.all(20),
                    child: Container(
                      height: 200,
                      width: double.infinity,
                      padding: const EdgeInsets.all(20),
                      child: Stack(
                        children: const [
                          Text(
                            'Rekomendasi',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Positioned(
                            top: 0,
                            right: 0,
                            child: Text(
                              'BISA',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    margin: const EdgeInsets.all(20),
                    child: Container(
                      height: 200,
                      width: double.infinity,
                      padding: const EdgeInsets.all(20),
                      child: Stack(
                        children: const [
                          Text(
                            'Rekomendasi',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Positioned(
                            top: 0,
                            right: 0,
                            child: Text(
                              'BISA',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomSearch extends SearchDelegate {
  List<String> allData = [
    'Test',
    'Test',
    'Test',
    'Test',
    'Test',
    'Tester',
    'Test',
    'Testing',
    'Tested',
  ];
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> matchQuery = [];
    for (var item in allData) {
      if (item.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(item);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<String> matchQuery = [];
    for (var item in allData) {
      if (item.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(item);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
  }
}
