import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  final double coverHeight = 230;
  final double profileHeight = 100;

  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(58),
        child: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: const Text("Profil"),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.logout_rounded),
              onPressed: () {},
            ),
          ],
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue, // Warna biru
              Colors.blue, // Warna biru
              Colors.white, // Warna putih
            ],
            stops: [0.5, 1 / 5, 0.5],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: ListView(
          padding: EdgeInsets.zero,
          // physics: ClampingScrollPhysics(),
          children: <Widget>[
            buildTop(),
            Padding(
              padding: const EdgeInsets.only(top: 2.0),
              child: Column(
                children: [
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          margin: const EdgeInsets.all(40),
                          child: Container(
                            height: 130,
                            width: double.infinity,
                            padding: const EdgeInsets.all(10),
                            child: Stack(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      "TENTANG SAYA",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    IconButton(
                                        onPressed: () {},
                                        icon: const Icon(Icons.edit)),
                                  ],
                                ),
                                const Positioned(
                                  top: 40,
                                  left: 5,
                                  right: 20,
                                  child: Text(
                                    "Aku adalah murid yang suka programming dan esport. Menikmati membangun program dan bermain game membuatku senang dan terus belajar.",
                                    style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          margin: const EdgeInsets.all(40),
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      "INFORMASI AKUN",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    IconButton(
                                      onPressed: () {},
                                      icon: const Icon(Icons.edit),
                                    ),
                                  ],
                                ),
                                // const SizedBox(height: 10),
                                const ListTile(
                                  title: Text(
                                    "Nama Lengkap",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Expanded(
                                    child: Text(
                                      "Putri Anindya",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 2,
                                ),
                                const ListTile(
                                  title: Text(
                                    "Nama Sekolah",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Expanded(
                                    child: Text(
                                      "SMP Therestial",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 2,
                                ),
                                const ListTile(
                                  title: Text(
                                    "Alamat",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Expanded(
                                    child: Text(
                                      "Semarang, Pedurungan",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 2,
                                ),
                                const ListTile(
                                  title: Text(
                                    "Nomor Telepon",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Expanded(
                                    child: Text(
                                      "+62 963-456-7890",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 2,
                                ),
                                const ListTile(
                                  title: Text(
                                    "Email",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Expanded(
                                    child: Text(
                                      "putriaja@gmail.com",
                                      textAlign: TextAlign.end,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTop() {
    final top = coverHeight - profileHeight / 2;

    return Container(
      // height: coverHeight,
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Positioned(
            top: top,
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [],
            ),
          ),
          buildCoverImage(),
          buildProfileImage(),
        ],
      ),
    );
  }

  Widget buildCoverImage() => Container(
      color: Colors.blue,
      child: const SizedBox(
        height: 300,
        width: double.infinity,
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  SizedBox(height: 205),
                  Text(
                    "Putri Anindya",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  // SizedBox(height: 5),
                  Text(
                    "SMP Therestial • kelas 7 ",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                        color: Colors.white),
                  ),
                  // SizedBox(height: 5),
                  Text(
                    "Semarang, Pedurungan",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
      ));

  Widget buildProfileImage() => Padding(
        padding: const EdgeInsets.only(top: 1),
        child: CircleAvatar(
          radius: 50,
          backgroundColor: Colors.grey.shade800,
          backgroundImage: const AssetImage("images/owi.jpg"),
        ),
      );
}
