import 'package:flutter/material.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(58),
          child: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
        ),
        backgroundColor: Colors.blue,
        body: Stack(
          children: [
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height * .4,
            //   decoration: const BoxDecoration(
            //     image: DecorationImage(
            //       image: AssetImage('images/Rectangle 270.png'),
            //       fit: BoxFit.cover,
            //     ),
            //   ),
            // ),
            // SizedBox(
            //   height: 100,
            //   width: MediaQuery.of(context).size.width - 40,
            //   child: const Text(
            //     "Cari Tahu Diri mu yang sesungguhnya",
            //     style: TextStyle(
            //         fontSize: 30,
            //         color: Color.fromARGB(255, 231, 231, 231),
            //         fontWeight: FontWeight.bold),
            //   ),
            // ),
            
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: MediaQuery.of(context).size.height * .7,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    // Padding(
                    //   padding: EdgeInsets.only(
                    //     top: 20,
                    //     left: 30,
                    //     right: 30,
                    //   ),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.start,
                    //     children: [
                    //       Icon(
                    //         Icons.star,
                    //         color: Colors.yellow,
                    //       ),
                    //       Icon(
                    //         Icons.star,
                    //         color: Colors.yellow,
                    //       ),
                    //       Icon(
                    //         Icons.star,
                    //         color: Colors.yellow,
                    //       ),
                    //       Icon(
                    //         Icons.star,
                    //         color: Colors.yellow,
                    //       ),
                    //       Icon(
                    //         Icons.star_half,
                    //         color: Colors.yellow,
                    //       ),
                    //       Text("4.5")
                    //     ],
                    //   ),
                    // ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: TabBar(
                        tabs: [
                          Text(
                            "Semua",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Trend",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Negeri",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            "Swasta",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                        labelColor: Colors.black,
                        unselectedLabelColor: Colors.grey,
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        children: [
                          // Photos tab content
                          Center(
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "Pekerjaan paling banyak dicari",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Text(
                              "Trend",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),

                          // Maps tab content
                          Center(
                            child: Text(
                              "Negeri",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),

                          // Reviews tab content
                          Center(
                            child: Text(
                              "Swasta",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
